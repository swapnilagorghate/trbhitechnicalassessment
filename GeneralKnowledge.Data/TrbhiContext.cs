namespace GeneralKnowledge.Data
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class TrbhiContext : DbContext
	{
		public TrbhiContext()
				: base("name=TrbhiContext")
		{
		}

		public virtual DbSet<Asset> Assets { get; set; }
		public virtual DbSet<Country> Countries { get; set; }
		public virtual DbSet<Mime_Type> Mime_Type { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Asset>()
					.Property(e => e.Asset_Id)
					.IsUnicode(false);

			modelBuilder.Entity<Asset>()
					.Property(e => e.File_Name)
					.IsUnicode(false);

			modelBuilder.Entity<Asset>()
					.Property(e => e.Created_By)
					.IsUnicode(false);

			modelBuilder.Entity<Asset>()
					.Property(e => e.Email)
					.IsUnicode(false);

			modelBuilder.Entity<Asset>()
					.Property(e => e.Description)
					.IsUnicode(false);

			modelBuilder.Entity<Country>()
					.Property(e => e.Name)
					.IsUnicode(false);

			modelBuilder.Entity<Mime_Type>()
					.Property(e => e.Name)
					.IsUnicode(false);

			modelBuilder.Entity<Mime_Type>()
					.HasMany(e => e.Assets)
					.WithOptional(e => e.Mime_Type)
					.HasForeignKey(e => e.MimeTypeId);
		}
	}
}
