﻿using System.Collections.Generic;

namespace GeneralKnowledge.Data
{
	public class AssetRepository : BaseRepository, IAssetRepository
	{
		public AssetRepository(TrbhiContext context) : base(context)
		{
		}

		public void Add(IEnumerable<Asset> assets)
		{
			_context.Assets.AddRange(assets);
			_context.SaveChanges();
		}

		public IEnumerable<Country> GetCountries()
		{
			return _context.Countries;
		}

		public IEnumerable<Mime_Type> GetMimeType()
		{
			return _context.Mime_Type;
		}
	}
}
