﻿using System.Collections.Generic;

namespace GeneralKnowledge.Data
{
	public interface IAssetRepository
	{
		void Add(IEnumerable<Asset> asset);
		//int GetCountryIdByName(string name);
		//int GetMimeTypeIdByName(string name);
		IEnumerable<Country> GetCountries();
		IEnumerable<Mime_Type> GetMimeType();
	}
}
