﻿using System;

namespace GeneralKnowledge.Data
{
	public class BaseRepository
	{
		#region Global Variables

		protected TrbhiContext _context;

		#endregion

		#region Constructor

		protected BaseRepository(TrbhiContext context)
		{
			_context = context;
		}

		#endregion

		#region Public Methods

		public void Save()
		{
			_context.SaveChanges();
		}

		#endregion

		#region Dispose

		private bool disposed;

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					_context.Dispose();
				}
			}
			disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		~BaseRepository()
		{
			Dispose(false);
		}

		#endregion
	}
}
