namespace GeneralKnowledge.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Asset")]
    public partial class Asset
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string Asset_Id { get; set; }

        [StringLength(500)]
        public string File_Name { get; set; }

        public int? MimeTypeId { get; set; }

        [StringLength(500)]
        public string Created_By { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int? CountryId { get; set; }

        public string Description { get; set; }

        public virtual Country Country { get; set; }

        public virtual Mime_Type Mime_Type { get; set; }
    }
}
