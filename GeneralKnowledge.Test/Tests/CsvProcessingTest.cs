﻿using GeneralKnowledge.Data;
using GeneralKnowledge.Test.App.Model;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;

namespace GeneralKnowledge.Test.App.Tests
{
	/// <summary>
	/// CSV processing test
	/// </summary>
	public class CsvProcessingTest : ITest
	{
		private IAssetRepository _assetRepository = new AssetRepository(new TrbhiContext());

		public void Run()
		{
			// TODO
			// Create a domain model via POCO classes to store the data available in the CSV file below
			// Objects to be present in the domain model: Asset, Country and Mime type
			// Process the file in the most robust way possible
			// The use of 3rd party plugins is permitted

			var csvFile = Resources.AssetImport;
			ReadCsv(csvFile);
		}

		private void ReadCsv(string csvFile)
		{
			// open the file "data.csv" which is a CSV file with headers
			using (CsvReader csv =
						 new CsvReader(new StreamReader(@"C:\00Swap\dotNET developer\GeneralKnowledge.Test\Resources\AssetImport.csv"), true))
			{
				var items = new List<Asset>();

				string[] headers = csv.GetFieldHeaders();
				var assets = new List<Asset>();

				Stopwatch sw = new Stopwatch();
				sw.Start();
				var countries = _assetRepository.GetCountries();
				var mimeTypes = _assetRepository.GetMimeType();
				while (csv.ReadNextRecord())
				{
					var asset = new Asset();
					asset.Asset_Id = csv[0];
					asset.File_Name = csv[1];
					asset.MimeTypeId = mimeTypes.FirstOrDefault(x=> x.Name.Equals(csv[2])).Id;
					asset.Created_By = csv[3];
					asset.Email = csv[4];
					asset.CountryId = countries.FirstOrDefault(x => x.Name.Equals(csv[5])).Id;
					asset.Description = csv[6];
					assets.Add(asset);
				}

				_assetRepository.Add(assets);
				sw.Stop();
				Console.WriteLine(sw.ElapsedMilliseconds);
			}
		}
	}
}
