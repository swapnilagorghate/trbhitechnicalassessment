﻿using System;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
	/// <summary>
	/// Basic string manipulation exercises
	/// </summary>
	public class StringTests : ITest
	{
		public void Run()
		{
			// TODO
			// Complete the methods below

			AnagramTest();
			GetUniqueCharsAndCount();
		}

		private void AnagramTest()
		{
			var word = "stop";
			var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

			foreach (var possibleAnagram in possibleAnagrams)
			{
				Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
			}
		}

		private void GetUniqueCharsAndCount()
		{
			// TODO
			// Write an algorithm that gets the unique characters of the word below 
			// and counts the number of occurrences for each character found
			var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";

			var uniqueCharacters = word
														.GroupBy(p => p)
														.Select(q =>
																		new
																		{
																			Character = q.Key,
																			Count = q.Count()
																		});

			foreach (var uniqueCharacter in uniqueCharacters)
			{
				Console.WriteLine($"Character: {uniqueCharacter.Character}, Count: {uniqueCharacter.Count}");
			}
		}
	}

	public static class StringExtensions
	{
		public static bool IsAnagram(this string a, string b)
		{
			// TODO
			// Write logic to determine whether a is an anagram of b
			return a.OrderBy(p => p).SequenceEqual(b.OrderBy(q => q));
		}
	}
}
