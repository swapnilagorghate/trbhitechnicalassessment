﻿using ImageResizer;
using System.IO;
using System.Net;
using System.Net.Http;

namespace GeneralKnowledge.Test.App.Tests
{
	/// <summary>
	/// Image rescaling
	/// </summary>
	public class RescaleImageTest : ITest
	{
		public void Run()
		{
			// TODO
			// Grab an image from a public URL and write a function that rescales the image to a desired format
			// The use of 3rd party plugins is permitted
			// For example: 100x80 (thumbnail) and 1200x1600 (preview)

			string imageUrl = "https://cdn.wonderfulengineering.com/wp-content/uploads/2014/10/wallpaper-photos-31.jpg";
			RescaleImage(imageUrl);
		}

		private void RescaleImage(string imageUrl)
		{
			byte[] imageData = null;

			using (var wc = new WebClient())
			{
				imageData = wc.DownloadData(imageUrl);
			}
			var inputStream = new MemoryStream(imageData);

			ImageJob job = new ImageJob(inputStream,
				@"C:\Users\swapnil\Desktop\img.png",
				new Instructions
				{
					Width = 192,
					Height = 108,
					Format = "jpg"
				});
			job.Build();
		}
	}
}
