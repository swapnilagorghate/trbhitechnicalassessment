﻿using GeneralKnowledge.Test.App.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
	/// <summary>
	/// Basic data retrieval from JSON test
	/// </summary>
	public class JsonReadingTest : ITest
	{
		public string Name { get { return "JSON Reading Test"; } }

		public void Run()
		{
			var jsonData = Resources.SamplePoints;

			// TODO: 
			// Determine for each parameter stored in the variable below, the average value, lowest and highest number.
			// Example output
			// parameter   LOW AVG MAX
			// temperature   x   y   z
			// pH            x   y   z
			// Chloride      x   y   z
			// Phosphate     x   y   z
			// Nitrate       x   y   z

			PrintOverview(jsonData);
		}

		private void PrintOverview(byte[] data)
		{
			string jsonStr = Encoding.UTF8.GetString(data);
			var jsonData = JsonConvert.DeserializeObject<Dictionary<string, Sample[]>>(jsonStr);
			foreach (var item in jsonData)
			{
				var samples = item.Value;

				Console.WriteLine("parameter    LOW AVG MAX");
				Console.WriteLine($"temperature  {samples.Min(c => c.temperature)}   {Math.Round(samples.Average(c => c.temperature), 2)}   {samples.Max(c => c.temperature)}");
				Console.WriteLine($"pH           {samples.Min(c => c.pH)}   { samples.Average(c => c.pH)}   {samples.Max(c => c.pH)}");
				Console.WriteLine($"Chloride     {samples.Min(c => c.chloride)}   {samples.Average(c => c.chloride)}   {samples.Max(c => c.chloride)}");
				Console.WriteLine($"Phosphate    {samples.Min(c => c.phosphate)}   {samples.Average(c => c.phosphate)}   {samples.Max(c => c.phosphate)}");
				Console.WriteLine($"Nitrate      {samples.Min(c => c.nitrate)}   {samples.Average(c => c.nitrate)}   {samples.Max(c => c.nitrate)}");
				
			}
		}
	}
}
