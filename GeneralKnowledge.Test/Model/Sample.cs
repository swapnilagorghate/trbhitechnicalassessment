﻿using System;

namespace GeneralKnowledge.Test.App.Model
{
	public class Sample
	{
		#region Properties

		public DateTime date { get; set; }

		public decimal temperature { get; set; }

		public int? pH { get; set; }

		public int? phosphate { get; set; }

		public int? chloride { get; set; }

		public int? nitrate { get; set; }

		#endregion
	}
}
